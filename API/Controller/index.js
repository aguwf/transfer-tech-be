var BookModel = require('../Model/BookModel')
var OwnerModel = require('../Model/OwnerModel')

const getBook = async (req, res) => {
  res.json({
    res: await BookModel.findById({ _id: '60fcd828f3c0cb5d9e3c4920' })
  })
}
const postBook = async (req, res) => {
  const nameBook = req.body
  try {
    const saveBook = await BookModel.create(nameBook)
    res.json(saveBook)
  } catch (error) {
    res.json({ message: error.message })
  }
}
const getOwner = async (req, res) => {
  try {
    const listOwner = await OwnerModel.find({}).populate({
      path: 'book',
      model: 'book',
      match: {}
    })
    res.json(listOwner)
  } catch (error) {
    res.send(error)
  }
}
const postOwner = async (req, res) => {
  //Tạo đối tượng owner từ dữ liệu nhận vào từ body
  let nameOwner = { name: req.body.name }
  try {
    //Gọi đến hàm create của model và truyền vào đối tượng vừa khởi tạo ở bước trên
    let newOwner = await OwnerModel.create(nameOwner)
    //Tìm đối tượng bằng id lấy được từ kết quả trả về của hàm create bằng hàm find
    let Owner = await OwnerModel.findById(newOwner._id)
    if (req.body.book) {
      //Kiểm tra xem có truyền vào book hay không
      let book = req.body.book //Lấy book và gán biến
      // book{
      //   "name": "abc"
      // }
      // Tạo ra 1 thuộc tính có tên là owner có giá trị là id của owner vừa tạo
      book.owner = newOwner._id
      // book{
      //   "name": "abc",
      //   owner: "9200148021983540923"
      // }
      //Gọi đến hàm create của BookModel để thêm mới book vào database
      let saveBook = await BookModel.create(book)

      // thêm id cảu sách vào owner
      Owner.book = saveBook._id
      await Owner.save()
    }

    res.json({ Owner, message: 'create success!' })
  } catch (error) {
    res.json({ message: error.message })
  }
}

const updateOwner = async (req, res) => {
  try {
    const id = req.params.ownerId
    const ownerName = req.body.name
    let book = req.body.book
    const Owner = await OwnerModel.findByIdAndUpdate(
      id,
      { name: ownerName },
      { new: true }
    )
    if (!book._id) {
      //Them 1 thuoc tinh owner trong doi tuong book voi gia tri la id cua owner
      book.owner = id
      // console.log(book)
      //Them vao db
      book = await BookModel.create(book)
      //Lay ra 1 owner theo id truyen vao
      if (Owner.book) {
        //TH owner da co book
        const oldBook = BookModel.findById(Owner.book._id)
        oldBook.owner = ''
        await oldBook.save()
      }
      // thay the id book moi vao book da co trong owner
      Owner.book = book._id
      await Owner.save()
    } else {
      //Lay thong tin sach theo id truyen vao
      book = await BookModel.findByIdAndUpdate(
        book._id,
        { name: book.name },
        { new: true }
      )
      // Xoa owner
      delete book.owner
      await book.save()
      // thay the id book moi vao book da co trong owner
      Owner.book = book._id
      await Owner.save()
    }

    // const Owner = await OwnerModel.findByIdAndUpdate(
    //   id,
    // {
    //   name: ownerName,
    //   book
    // },
    // { new: true }
    // )
    res.json(Owner)
  } catch (error) {
    console.log(error)
    res.send({ error })
  }
}

const uploadFile = (req, res) => {}

const uploadMultiFile = (req, res) => {
  console.log(req.files)
  const file = req.files[0]
  res.json({ Url: `http://localhost:3001/${file.originalname}` })
}

module.exports = {
  getBook,
  postBook,
  getOwner,
  postOwner,
  updateOwner,
  uploadFile,
  uploadMultiFile
}
