var mongoose = require('mongoose')
var Schema = mongoose.Schema
var model = mongoose.model

const OwnerSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  book: {
    type: mongoose.Types.ObjectId,
    ref: 'book'
  }
})

module.exports = model('owner', OwnerSchema)
