var mongoose = require('mongoose')
var Schema = mongoose.Schema
var model = mongoose.model

const BookSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  owner: {
    type: mongoose.Types.ObjectId,
    ref: 'owner'
  }
})

module.exports = model('book', BookSchema)
